#include<bits/stdc++.h>
using namespace std;

long long n;
int m;
int mm;
int mod=1000000007;

//目前是x但是填满此阶后，下一阶是y的可能有几种 
int next[128][128]={0};

int result[128][128]={0};
int temp[128][128]={0};

//1.*   2. *  3.**  4.**
//  **    **    *      *
void dfs(int nowNumber,int nowValue,int nextValue,int i)
{
	if(i>=m)
	{
		if((nowValue==mm-1)&&(nextValue<mm))
		{
			next[nowNumber][nextValue]++;
		}
		return;
	}
	
	//可以不放方块 
	if(((nowValue>>i)&1)==1)
	{
		dfs(nowNumber,nowValue,nextValue,i+1);
	}
	
	//可以放第一种方块
	if( ((nowValue>>i)&1)==0 && ((nowValue>>(i+1))&1)==0 && ((nextValue>>i)&1)==0 ) 
	{
		dfs(nowNumber,nowValue|(1<<i)|(1<<(i+1)),nextValue|(1<<i),i+1);
	}
	
	//可以放第二种方块 
	if( ((nowValue>>i)&1)==0 && ((nowValue>>(i+1))&1)==0 && ((nextValue>>(i+1))&1)==0 )
	{
		dfs(nowNumber,nowValue|(1<<i)|(1<<(i+1)),nextValue|(1<<(i+1)),i+1);
	}
	
	//可以放第三种方块
	if( ((nowValue>>i)&1)==0 && ((nextValue>>i)&1)==0 && ((nextValue>>(i+1))&1)==0 ) 
	{
		dfs(nowNumber,nowValue|(1<<i),nextValue|(1<<i)|(1<<(i+1)),i+1);
	}
	
	//可以放第四种方块
	if( ((nowValue>>i)&1)==1 && ((nowValue>>(i+1))&1)==0 && ((nextValue>>i)&1)==0 && ((nextValue>>(i+1))&1)==0) 
	{
		dfs(nowNumber,nowValue|(1<<(i+1)),nextValue|(1<<i)|(1<<(i+1)),i+1);
	}
}

void input()
{
	cin>>n>>m;
	
	mm=1<<m;
	
	for(int i=0;i<mm;i++)
	{
		dfs(i,i,0,0);
	}
	
}

//两矩阵乘积 
void mul(int one[][128],int two[][128])
{
	memset(temp,0,sizeof(temp));
	
	for(int i=0;i<mm;i++)
	{
		for(int j=0;j<mm;j++)
		{
			for(int k=0;k<mm;k++)
			{
				temp[i][j]=(temp[i][j]+(long long)one[i][k]*two[k][j])%mod;
			}
		}
	}
	
	memcpy(one,temp,sizeof(temp));
}

int main()
{
	input();

	//单位矩阵 
	for(int i=0;i<mm;i++)
	{
		for(int j=0;j<mm;j++)
		{
			result[i][j]=(i==j);
		}
	}
	
	//快速幂 
	long long l=n-1;
	while(l)
	{
		if(l%2==1)
		{
			mul(result,next);
		}
		mul(next,next);
		l/=2;
	}

	cout<<result[0][mm-1];

	return 0;
}
