#计算next二维数组函数
def cal_next(nownumber,nowvalue,nextvalue,i):
    global mm
    global m
    global next

    if i >= m:
        if nowvalue == mm - 1 and nextvalue < mm:
            next[nownumber][nextvalue]+=1
        return

    #可以不放方块
    if ((nowvalue >> i) & 1) == 1:
        cal_next(nownumber,nowvalue,nextvalue,i + 1)

    #*
    #**
    #可以放第一种方块
    if ((nowvalue >> i) & 1) == 0 and ((nowvalue >> (i + 1)) & 1) == 0 and ((nextvalue >> i) & 1) == 0:
        cal_next(nownumber,nowvalue | (1 << i) | 1 << (i + 1),nextvalue | (1 << i),i + 1)

    # *
    #**
    #可以放第二种方块
    if ((nowvalue >> i) & 1) == 0 and ((nowvalue >> (i + 1)) & 1) == 0 and ((nextvalue >> (i + 1)) & 1) == 0:
        cal_next(nownumber,nowvalue | (1 << i) | (1 << (i + 1)),nextvalue | (1 << (i + 1)),i + 1)

    #**
    #*
    #可以放第三种方块
    if ((nowvalue >> i) & 1) == 0 and ((nextvalue >> i) & 1) == 0 and ((nextvalue >> (i + 1)) & 1) == 0:
        cal_next(nownumber,nowvalue | (1 << i),nextvalue | (1 << i) | (1 << (i + 1)),i + 1)

    #**
    # *
    #可以放第四种方块
    if ((nowvalue >> (i + 1)) & 1) == 0 and ((nextvalue >> i) & 1) == 0 and ((nextvalue >> (i + 1)) & 1) == 0:
        cal_next(nownumber,nowvalue | (1 << (i + 1)),nextvalue | (1 << i) | (1 << (i + 1)),i + 1)

#计算两矩阵乘积
def mul(rect1,rect2,mm):
    r = [[0 for i in range(0,128)] for j in range(0,128)]

    for i in range(0,mm):
        for j in range(0,mm):
            for k in range(0,mm):
                r[i][j] = (r[i][j] + rect1[i][k] * rect2[k][j]) % mod
    
    for i in range(0,mm):
        for j in range(0,mm):
            rect1[i][j] = r[i][j]

n,m = map(int,input().split())

if (n * m) % 3 != 0:
    print(0)
    exit(0)

if n < m:
    temp = n
    n = m
    m = temp

mm = (1 << m)
mod = 1000000007

next = [[0 for i in range(0,128)] for j in range(0,128)]

#计算出next二维数组
for i in range(0,mm):
    cal_next(i,i,0,0)

#初始化为单位矩阵
result = [[1 if i == j else 0 for i in range(0,128)] for j in range(0,128)]

#快速幂
l = n - 1
while l > 0:
    if (l & 1) == 1:
        mul(result,next,mm)
    mul(next,next,mm)
    l = int(l >> 1)
print(result[0][mm - 1])
exit(0)
